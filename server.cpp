#include <iostream>
#include "gen/abstractstubserver.h"
#include "database/iterator.h"
#include "database/table.h"
#include "database/data.h"
#include "database/database.h"
#include <jsonrpccpp/server/connectors/httpserver.h>
#include <cstdio>

using namespace jsonrpc;
using namespace std;

class MyStubServer : public AbstractStubServer {
public:
    MyStubServer(AbstractServerConnector &connector, serverVersion_t type) : AbstractStubServer(connector, type) {};
    virtual std::string my_open(const std::string& dbName, bool overwrite);
    virtual std::string my_create(const Json::Value& paramsList, const std::string& tableName);
    virtual std::string my_insert(const Json::Value& paramsList, const std::string& tableName);
    virtual std::string my_update(const std::string& tableName, const std::string& updateColName,
                                  const std::string& updateColType, const std::string& updateVal,
                                  const std::string& whereColName, const std::string& whereColType,
                                  const std::string& whereVal);
    virtual std::string my_delete(const std::string& columnName, const std::string& columnType,
                                  const std::string& tableName, const std::string& value);
    virtual std::string my_select_all(const std::string& tableName);
    virtual std::string my_select(const std::string& columnName, const std::string& columnType,
                                  const std::string& tableName, const std::string& value);
    virtual void my_close() {
        close_database(this->db);
        this->db = nullptr;
    }
private:
    database* db = nullptr;
    my_iterator *last_iter = nullptr;
};

string MyStubServer::my_open(const std::string &dbName, bool overwrite) {
    if (dbName.empty()) {
        cout << "db name empty\n";
        exit(1);
    }

    FILE* fp = fopen(dbName.c_str(), "wb+");
    if (!fp) {
        cout << "Unable to open file pointer\n";
        exit(1);
    }
    db = init_database(fp, overwrite);

    return "ok";
}

string MyStubServer::my_create(const Json::Value &paramsList, const std::string &tableName) {
    if (!paramsList.isArray()) throw JsonRpcException(-1, "No list of args");
    if (tableName.empty()) throw JsonRpcException(-1, "Table name empty");

    table *t = new_table(tableName.c_str(), paramsList.size()/2);

    for (int i = 0 ; i < paramsList.size() / 2 ; i++) {
        if (paramsList[i*2].asString() == "VARCHAR") {
            init_var_column(t, paramsList.size()/2 - i - 1, paramsList[i*2 + 1].asCString(), VARCHAR, 50);
        } else if (paramsList[i*2].asString() == "INT") {
            init_fixed_column(t, paramsList.size()/2 - i - 1, paramsList[i*2 + 1].asCString(), INT);
        } else if (paramsList[i*2].asString() == "DOUBLE") {
            init_fixed_column(t, paramsList.size()/2 - i - 1, paramsList[i*2 + 1].asCString(), DOUBLE);
        } else if (paramsList[i*2].asString() == "BOOLEAN") {
            init_fixed_column(t, paramsList.size()/2 - i - 1, paramsList[i*2 + 1].asCString(), BOOLEAN);
        } else {
            throw JsonRpcException(1, "Unknown column type: " + paramsList[i*2].asString());
        }

    }
    table_commit(this->db, t);
    close_table(t);

    return "ok";
}

std::string MyStubServer::my_insert(const Json::Value &paramsList, const std::string &tableName) {
    if (!paramsList.isArray()) throw JsonRpcException(-1, "No list of args");
    if (tableName.empty()) throw JsonRpcException(-1, "Table name empty");

    table *t = open_table(this->db, tableName.c_str());
    if (t->header->column_num != paramsList.size()/2) throw JsonRpcException(-1, "num of passed parameters doesn't match one in table");

    data *d = init_data(t);
    for (int i = 0 ; i < paramsList.size()/2 ; i++) {
        if (paramsList[i*2].asString() == "VARCHAR") {
            init_varchar(d, paramsList[i*2+1].asCString(), t->header->columns[i].name);
        } else if (paramsList[i*2].asString() == "INT") {
            init_int(d, (int32_t) atoi(paramsList[i*2+1].asCString()), t->header->columns[i].name);
        } else if (paramsList[i*2].asString() == "DOUBLE") {
            init_double(d, atof(paramsList[i*2+1].asCString()), t->header->columns[i].name);
        } else if (paramsList[i*2].asString() == "BOOLEAN") {
            init_boolean(d, strcmp(paramsList[i*2+1].asCString(), "1") == 0, t->header->columns[i].name);
        } else {
            throw JsonRpcException(-1, "unknown data type");
        }
    }

    if (insert_data(d, this->db)) {
        return "ok";
    } else {
        return "not inserted";
    }
}

std::string MyStubServer::my_update(const std::string& tableName, const std::string& updateColName,
                                    const std::string& updateColType, const std::string& updateVal,
                                    const std::string& whereColName, const std::string& whereColType,
                                    const std::string& whereVal) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    if (updateColName.empty()) throw JsonRpcException(-1, "Update col name empty");
    if (updateColType.empty()) throw JsonRpcException(-1, "Update col type empty");
    if (updateVal.empty()) throw JsonRpcException(-1, "Update val empty");
    if (whereColName.empty()) throw JsonRpcException(-1, "Where col name empty");
    if (whereColType.empty()) throw JsonRpcException(-1, "Where col type empty");
    if (whereVal.empty()) throw JsonRpcException(-1, "Where val empty");

    table *t = open_table(this->db, tableName.c_str());

    int32_t where_int, update_int;
    double where_double, update_double;
    bool where_bool, update_bool;
    void *where_value, *update_value;
    column_type where_type, update_type;

    if (strcmp(whereColType.c_str(), "INT") == 0) {
        where_type = INT;
        where_int = atoi(whereVal.c_str());
        where_value = (void*) &where_int;
    } else if (strcmp(whereColType.c_str(), "VARCHAR") == 0) {
        where_type = VARCHAR;
        where_value = (void*) whereVal.c_str();
    } else if (strcmp(whereColType.c_str(), "DOUBLE") == 0) {
        where_type = DOUBLE;
        where_double = atof(whereVal.c_str());
        where_value = (void*) &where_double;
    } else if (strcmp(whereColType.c_str(), "BOOLEAN") == 0) {
        where_type = BOOLEAN;
        where_bool = strcmp(whereVal.c_str(), "true") == 0;
        where_value = (void*) &where_bool;
    } else {
        return "wrong where data type";
    }

    if (strcmp(updateColType.c_str(), "INT") == 0) {
        update_type = INT;
        update_int = atoi(updateVal.c_str());
        update_value = (void*) &update_int;
    } else if (strcmp(updateColType.c_str(), "VARCHAR") == 0) {
        update_type = VARCHAR;
        update_value = (void*) updateVal.c_str();
    } else if (strcmp(updateColType.c_str(), "DOUBLE") == 0) {
        update_type = DOUBLE;
        update_double = atof(updateVal.c_str());
        update_value = (void*) &update_double;
    } else if (strcmp(updateColType.c_str(), "BOOLEAN") == 0) {
        update_type = BOOLEAN;
        update_bool = strcmp(updateVal.c_str(), "true") == 0;
        update_value = (void*) &update_bool;
    } else {
        return "wrong update data type";
    }

    uint32_t upd = update_where(db, t, whereColName.c_str(), where_type,
                           where_value, updateColName.c_str(), update_type,
                           update_value);
    if (upd > 0) {
        return "updated " + to_string(upd) + " rows";
    } else {
        return "no rows updated";
    }
}

std::string MyStubServer::my_delete(const std::string &columnName, const std::string &columnType,
                                    const std::string &tableName, const std::string &value) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name empty");
    if (columnName.empty()) throw JsonRpcException(-1, "Column name empty");
    if (columnType.empty()) throw JsonRpcException(-1, "Column type empty");
    if (value.empty()) throw JsonRpcException(-1, "Value empty");

    table *t = open_table(this->db, tableName.c_str());

    column_type type;
    void* real_value;
    int32_t i;
    double d;
    bool b;
    if (columnType == "VARCHAR") {
        type = VARCHAR;
        real_value = malloc(sizeof(value.c_str()));
        strcpy((char*) real_value, value.c_str());
    } else if (columnType == "INT") {
        type = INT;
        i = atoi(value.c_str());
        real_value = (void*) &i;
    } else if (columnType == "DOUBLE") {
        type = DOUBLE;
        d = atof(value.c_str());
        real_value = (void*) &d;
    } else if (columnType == "BOOLEAN") {
        b = value == "true";
        real_value = (void*) &b;
        type = BOOLEAN;
    }

    uint32_t num = delete_where(this->db, t, columnName.c_str(), type, real_value);
    if (num > 0) {
        return "deleted " + to_string(num) + " rows";
    }

    return "deleted 0 rows";
}

std::string MyStubServer::my_select_all(const std::string &tableName) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name empty");

    table *t = open_table(this->db, tableName.c_str());
    my_iterator *iter = init_iterator(this->db, t);

    int counter = 0;
    string result;
    string str_val_cpp;
    char *str_val;

    while (seek_next(iter)) {
        if (counter == 0) {
            for (int i = 0 ; i < t->header->column_num ; i++) {
                result += t->header->columns[i].name;
                result += "  ";
            }
            result += "\n";
        }

        for (int i = 0 ; i < t->header->column_num ; i++) {
            switch (t->header->columns[i].type) {
                case INT:
                    int32_t int_val;
                    get_int(iter, t->header->columns[i].name, &int_val);
                    result += to_string(int_val);
                    break;
                case VARCHAR:
                    str_val = (char*) malloc(sizeof(char) * 300);
                    get_varchar(iter, t->header->columns[i].name, &str_val);
                    str_val_cpp = string(str_val);
                    free(str_val);
                    result += str_val_cpp;
                    break;
                case BOOLEAN:
                    bool bool_val;
                    get_bool(iter, t->header->columns[i].name, &bool_val);
                    result += bool_val ? "true" : "false";
                    break;
                case DOUBLE:
                    double double_val;
                    get_double(iter, t->header->columns[i].name, &double_val);
                    result += to_string(double_val);
                    break;
            }
            result += "  ";
        }

        result += "\n";
        counter++;
    }

    return result;
}

std::string MyStubServer::my_select(const std::string &columnName, const std::string &columnType,
                                    const std::string &tableName, const std::string &value) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name empty");
    if (columnName.empty()) throw JsonRpcException(-1, "Column name empty");
    if (columnType.empty()) throw JsonRpcException(-1, "Column type empty");
    if (value.empty()) throw JsonRpcException(-1, "Value empty");

    table *t = open_table(this->db, tableName.c_str());
    this->last_iter = init_iterator(this->db, t);

    int counter = 0;
    string result;
    column_type type;
    void* val_to_search;
    double  double_val;
    int     int_val;
    bool    bool_val;

    if (columnType == "VARCHAR") {
        type = VARCHAR;
        val_to_search = (void*) value.c_str();
    } else if (columnType == "DOUBLE") {
        type = DOUBLE;
        double_val = atof(value.c_str());
        val_to_search = (void*) &double_val;
    } else if (columnType == "INT") {
        type = INT;
        int_val = atoi(value.c_str());
        val_to_search = (void*) &int_val;
    } else if (columnType == "BOOLEAN") {
        type = BOOLEAN;
        bool_val = value == "true";
        val_to_search = &bool_val;
    } else {
        throw JsonRpcException(-1, "wrong column type");
    }

    while (seek_next_where(this->last_iter, columnName.c_str(), type, val_to_search)) {
        if (counter == 0) {
            for (int i = 0 ; i < t->header->column_num ; i++) {
                result += t->header->columns[i].name;
                result += "\t";
            }
            result += "\n";
        }

        for (int i = 0 ; i < t->header->column_num ; i++) {
            switch (t->header->columns[i].type) {
                case INT:
                    int_val = 0;
                    result += to_string(get_int(this->last_iter, t->header->columns[i].name, &int_val));
                    break;
                case VARCHAR:
                    char *str_val[50];
                    result += to_string(get_varchar(this->last_iter, t->header->columns[i].name, str_val));
                    break;
                case BOOLEAN:
                    bool_val = false;
                    result += get_bool(this->last_iter, t->header->columns[i].name, &bool_val) ? "true" : "false";
                    break;
                case DOUBLE:
                    double_val = 0;
                    result += to_string(get_double(this->last_iter, t->header->columns[i].name, &double_val));
                    break;
            }
            result += "  ";
        }

        result += "\n";
        counter++;

        if (!has_next(this->last_iter)) {
            free(this->last_iter);
            this->last_iter = nullptr;
        }
    }

    return result;
}

int main(int argc, char *argv[]) {
    if (argc < 4 || strcmp(argv[1], "-help") == 0) {
        cout << "format: ./server <port> <db_file> <overwrite (0/1)>" << endl;
        return 1;
    }
    int port = atoi(argv[1]);
    printf("port: %s\ndatabase file: %s\noverwrite: %s\n\n", argv[1], argv[2], argv[3]);
    HttpServer httpserver(port);
    MyStubServer s(httpserver,
                   JSONRPC_SERVER_V2);
    s.StartListening();
    try {
        s.my_open(argv[2], strcmp(argv[3], "1") == 0);
    } catch (JsonRpcException e) {
        cout << e.what() << endl;
        s.StopListening();
        return 1;
    }
    cout << "server is running, press enter to stop\n";
    getchar();
    s.my_close();
    s.StopListening();
}
