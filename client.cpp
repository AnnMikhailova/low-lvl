#include <iostream>

#include "gen/stubclient.h"
#include <jsonrpccpp/client/connectors/httpclient.h>
#include <algorithm>
#include "parser_gen/parser.tab.h"
#include "parser_gen/tree.h"

using namespace jsonrpc;
using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2 || strcmp(argv[1], "-help") == 0) {
        cout << "format: ./client <port>" << endl;
    }

    cout << "ready to parse..." << endl;
    string url = "http://localhost:";
    url += argv[1];
    HttpClient httpClient(url);
    StubClient c(httpClient, JSONRPC_CLIENT_V2);
    struct node *n = nullptr;

    while (true) {
        try {
            cout << "\n > ";
            n = yyparse();
            if (n == nullptr) throw JsonRpcException(-1, "query wasn't parsed");
            if (n->type == SELECT_QUERY) {
                string table_name = n->main.name;
                string column_name, column_type, value;

                if (n->left->type == CREATE_LIST && n->left->left == NULL && n->left->right == NULL) {
                    cout << c.my_select_all(table_name);
                } else {
                    column_name = n->right->left->additional.name;
                    if (n->right->right->type == INT_NODE) {
                        column_type = "INT";
                        value = to_string(n->right->right->main.int_val);

                    } else if (n->right->right->type == DOUBLE_NODE) {
                        column_type = "DOUBLE";
                        value = to_string(n->right->right->main.double_val);
                    } else if (n->right->right->type == BOOLEAN_NODE) {
                        column_type = "BOOLEAN";
                        value = to_string(n->right->right->main.boolean_val);
                    } else if (n->right->right->type == VARCHAR_NODE) {
                        column_type = "VARCHAR";
                        value = n->right->right->main.varchar_val;
                    }
                    cout << c.my_select(column_name, column_type, table_name, value) << endl;
                }
            } else if (n->type == DELETE_QUERY) {
                string table_name = n->main.name;
                string column_name = n->left->left->additional.name;
                string column_type, value;

                if (n->left->right->type == INT_NODE) {
                    column_type = "INT";
                    value = to_string(n->left->right->main.int_val);
                } else if (n->left->right->type == DOUBLE_NODE) {
                    column_type = "DOUBLE";
                    value = to_string(n->left->right->main.double_val);
                } else if (n->left->right->type == BOOLEAN_NODE) {
                    column_type = "BOOLEAN";
                    value = to_string(n->left->right->main.boolean_val);
                } else if (n->left->right->type == VARCHAR_NODE) {
                    column_type = "VARCHAR";
                    value = n->left->right->main.varchar_val;
                }
                cout << c.my_delete(column_name, column_type, table_name, value) << endl;
            } else if (n->type == UPDATE_QUERY) {
                string table_name = n->main.name;

                string update_col_name = n->right->left->additional.name;
                string where_col_name = n->right->left->additional.name;
                string update_col_type, where_col_type;
                string update_val, where_val;
                if (n->center->left->type == VARCHAR_NODE) {
                    where_col_type = "VARCHAR";
                    where_val = n->right->right->main.varchar_val;
                    update_col_type = "VARCHAR";
                    update_val = n->center->left->main.varchar_val;
                } else if (n->center->left->type == DOUBLE_NODE) {
                    where_col_type = "DOUBLE";
                    where_val = to_string(n->right->right->main.double_val);
                    update_col_type = "DOUBLE";
                    update_val = to_string(n->center->left->main.double_val);
                } else if (n->center->left->type == INT_NODE) {
                    where_col_type = "INT";
                    where_val = to_string(n->right->right->main.int_val);
                    update_col_type = "INT";
                    update_val = to_string(n->center->left->main.int_val);
                } else if (n->center->left->type == BOOLEAN_NODE) {
                    where_col_type = "BOOLEAN";
                    where_val = n->right->right->main.boolean_val == 1 ? "true" : "false";
                    update_col_type = "BOOLEAN";
                    update_val = n->center->left->main.boolean_val == 1 ? "true" : "false";
                }
                cout << c.my_update(table_name, update_col_name, update_col_type,
                                    update_val, where_col_name, where_col_type,
                                    where_val);
            } else if (n->type == INSERT_QUERY) {
                string table_name = n->main.name;
                Json::Value paramsList(Json::arrayValue);
                vector<string> v;
                node *current = n->left;
                while (current != nullptr) {
                    if (current->type == CREATE_LIST && current->right != nullptr) {
                        if (current->right->type == INT_NODE) {
                            v.push_back(to_string(current->right->main.int_val));
                            v.push_back("INT");
                        } else if (current->right->type == DOUBLE_NODE) {
                            v.push_back(to_string(current->right->main.double_val));
                            v.push_back("DOUBLE");
                        } else if (current->right->type == VARCHAR_NODE) {
                            v.push_back(current->right->main.varchar_val);
                            v.push_back("VARCHAR");
                        } else if (current->right->type == BOOLEAN_NODE) {
                            v.push_back(to_string(current->right->main.boolean_val));
                            v.push_back("BOOLEAN");
                        }
                    } else if (current->type == CREATE_LIST && current->right == nullptr) {
                        if (current->left->type == INT_NODE) {
                            v.push_back(to_string(current->left->main.int_val));
                            v.push_back("INT");
                        } else if (current->left->type == DOUBLE_NODE) {
                            v.push_back(to_string(current->left->main.double_val));
                            v.push_back("DOUBLE");
                        } else if (current->left->type == VARCHAR_NODE) {
                            v.push_back(current->left->main.varchar_val);
                            v.push_back("VARCHAR");
                        } else if (current->left->type == BOOLEAN_NODE) {
                            v.push_back(to_string(current->left->main.boolean_val));
                            v.push_back("BOOLEAN");
                        }
                    }
                    current = current->left;
                }

                reverse(v.begin(), v.end());
                for (const auto & i : v) paramsList.append(i);
                cout << c.my_insert(paramsList, table_name);
            } else if (n->type == CREATE_QUERY) {
                string table_name = n->main.name;
                Json::Value paramsList(Json::arrayValue);
                vector<string> v;
                node *current = n->left;
                while (current != nullptr) {
                    if (current->type == CREATE_LIST && current->right != nullptr) {
                        v.push_back(data_type_string[current->right->left->main.data_type]);
                        v.push_back(current->right->main.name);
                    } else if (current->type == CREATE_COLUMN) {
                        v.push_back(data_type_string[current->left->main.data_type]);
                        v.push_back(current->main.name);
                    }
                    current = current->left;
                }
                for (const auto & i : v) paramsList.append(i);
                cout << c.my_create(paramsList, table_name) << endl;
            } else {
                throw JsonRpcException(-1, "unsupported query type");
            }

        } catch (JsonRpcException &e) {
            cerr << e.what() << endl;
        }
    }

    return 0;
}