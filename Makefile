all:
	make client
	make server

client: parser_gen/lex.yy.c parser_gen/parser.tab.c parser_gen/tree.c client.cpp
	g++ -std=c++14 client.cpp database/table.c database/file_api.c database/data.c database/iterator.c parser_gen/*.c -o client

server: server.cpp database/table.c database/file_api.c database/data.c database/iterator.c
	g++ -std=c++14 server.cpp database/table.c database/file_api.c database/data.c database/iterator.c -o server

