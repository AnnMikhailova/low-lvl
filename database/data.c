#include "data.h"

void close_data(struct data* dt) {
    free(dt->header);
    free(dt->byte_data);
    free(dt);
}

struct data* init_data(struct table* tb) {
    data* dt = (data*) malloc(sizeof(data));
    data_header *header = (data_header*) malloc(sizeof(data_header));
    header->valid = true;
    header->next_invalid_row_page = -1;
    header->next_invalid_row_index = -1;
    dt->t = tb;
    dt->byte_data = (void**) malloc(tb->header->row_size);
    memcpy(dt->byte_data, header, sizeof(data_header));
    dt->ptr = sizeof(data_header);
    dt->header = header;
    return dt;
}

void init_int(struct data* dt, int32_t val, const char* col_name) {
    column_header *header = get_column_header_by_name(dt->t, col_name);
    char* ptr = (char*) dt->byte_data + dt->ptr;
    *((int32_t*) (ptr)) = val;
    dt->ptr += header->size;
}

void init_varchar(struct data* dt, const char* val, const char* col_name) {
    column_header *header = get_column_header_by_name(dt->t, col_name);
    char* ptr = (char*) dt->byte_data + dt->ptr;
    strcpy((char *) (ptr), val);
    dt->ptr += header->size;
}

void init_boolean(struct data* dt, bool val, const char* col_name) {
    column_header *header = get_column_header_by_name(dt->t, col_name);
    char* ptr = (char*) dt->byte_data + dt->ptr;
    *((bool*) (ptr)) = val;
    dt->ptr += header->size;
}

void init_double(struct data* dt, double val, const char* col_name) {
    column_header *header = get_column_header_by_name(dt->t, col_name);
    char* ptr = (char*) dt->byte_data + dt->ptr;
    *((double*) (ptr)) = val;
    dt->ptr += header->size;
}

void init_any(struct data* dt, column_type type, void* value, char* col_name) {
    switch (type) {
        case INT:
            init_int(dt, *((int32_t*) value), col_name);
            break;
        case VARCHAR:
            init_varchar(dt, *((char**) value), col_name);
            break;
        case BOOLEAN:
            init_boolean(dt, *((bool*) value), col_name);
            break;
        case DOUBLE:
            init_double(dt, *((double*) value), col_name);
            break;
        default:
            return;
    }

}

bool insert_data(struct data *d, struct database *db) {
    bool success = false;
    page *pg = d->t->first_writable_page;
    uint32_t offset = d->t->header->row_size * pg->header->num_rows;
    memcpy(pg->byte_data + offset, d->byte_data, d->ptr);
    pg->header->num_rows++;
    pg->header->free_row_offset += pg->t_header->row_size;
    if (!has_space(pg)) {
        pg->header->is_free = false;
        d->t->first_writable_page = page_alloc(db);
        pg->header->next_page_index = d->t->first_writable_page->header->page_index;
        table_to_page_link(d->t->first_writable_page, d->t);
        if (!pg->next_page) {
            pg->next_page = d->t->first_writable_page;
        }
        if (pg != d->t->first_page) {
            if (db) {
                success = write_page(db->fp, pg);
                write_page(db->fp, d->t->first_writable_page);
                close_page(pg);
            }
            close_data(d);
            return success;
        }
    }
    success = write_page(db->fp, pg);
    close_data(d);
    return success;
}
