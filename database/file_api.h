#ifndef FILE_API_H
#define FILE_API_H

#define PAGE_SIZE 4096

#include "table.h"
#include "database.h"

struct page {
    struct page_header *header;
    struct table_header *t_header;
    struct page *next_page;
    void* byte_data; // intentional asterisk next to type
};

typedef struct page page;

struct page_header {
    bool is_free;
    uint64_t page_index;
    uint64_t next_page_index;
    uint32_t num_rows;
    uint32_t free_row_offset;
};

typedef struct page_header page_header;

struct database *init_database(FILE *fp, bool overwrite);
void close_database(struct database *db);
bool write_database_header(FILE *fp, struct database_header *db_header);
struct page *page_alloc(struct database *db);
struct page *read_page(struct database *db, uint64_t page_index);
struct page *get_first_free_page(struct database *db, struct table *t);
bool write_page(FILE *fp, struct page *pg);
bool has_space(struct page *pg);
void close_page(struct page *pg);
void table_to_page_link(struct page *pg, struct table *t);

#endif
