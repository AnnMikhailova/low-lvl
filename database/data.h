#ifndef LLP1_DATA_H
#define LLP1_DATA_H

/*
 * Implementation of functions used for INSERT operation(s)
 */


#include "table.h"

struct __attribute__((packed)) data_header {
    bool valid;
    int32_t next_invalid_row_page;
    int32_t next_invalid_row_index;
};

typedef struct data_header data_header;

struct data {
    struct data_header *header;
    struct table *t;
    void** byte_data; //intentional double asterisk next to type
    uint16_t ptr;
};

typedef struct data data;

struct data* init_data(struct table *tb);
void init_int(struct data *dt, int32_t val, const char *col_name);
void init_varchar(struct data *dt, const char *val, const char *col_name);
void init_boolean(struct data *dt, bool val, const char *col_name);
void init_double(struct data *dt, double val, const char *col_name);
void init_any(struct data *dt, column_type type, void *value, char *col_name);
bool insert_data(struct data *d, struct database * db);

#endif //LLP1_DATA_H
