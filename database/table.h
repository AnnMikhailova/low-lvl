#ifndef TABLE_H
#define TABLE_H

#define NAME_LEN 20
#define VARCHAR_LEN 255

#include "database.h"
#include "file_api.h"
#include <string.h>
#include <stdlib.h>

enum column_type {
    INT,
    VARCHAR,
    DOUBLE,
    BOOLEAN
};

typedef enum column_type column_type;

struct column_header {
    column_type type;
    uint32_t size;
    char name[NAME_LEN];
};

typedef struct column_header column_header;

struct table_header {
    uint32_t size;
    uint16_t row_size;
    uint16_t column_num;
    uint16_t name_len;
    char name[NAME_LEN];
    struct column_header columns[];
};

typedef struct table_header table_header;

struct table {
    struct table_header *header;
    struct database *database;
    struct page *first_page;
    struct page *first_writable_page;
};

typedef struct table table;

struct column {
    struct column_header *header;
    char *name;
};

typedef struct column column;

struct table *new_table(const char *name, uint8_t num_columns);
struct table *open_table(struct database *db, const char* name);
void close_table(struct table *tb);
uint8_t type_to_column_size(column_type type);
void table_commit(struct database *db, struct table *t);
void init_fixed_column(struct table *tb, uint16_t index, const char *name, column_type type);
void init_var_column(struct table *tb, uint16_t index, const char *name, column_type type, uint32_t size);
struct column_header *get_column_header_by_name(struct table *tb, const char *name);

#endif
