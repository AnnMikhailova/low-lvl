#include "tree.h"

struct node *new_select_query(const char *table, const char *entity, struct node *columns, struct node *filter) {
    struct node *n = (struct node*) malloc(sizeof(node));
    n->type = SELECT_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table);
    n->additional.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->additional.name, entity);
    n->left = columns;
    n->right = filter;
    n->center = NULL;
    return n;
}

struct node *new_join_query(const char *left_table_name, const char *left_entity_name, const char *right_table_name,
                     const char *right_entity_name, struct node *left_column, node *right_column, struct node *column_list) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = JOIN_QUERY;

    n->main.name = (char*) malloc(sizeof(char) * 255);
    n->additional.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, left_table_name);
    strcpy(n->additional.name, left_entity_name);

    n->main_second.name = (char*) malloc(sizeof(char) * 255);
    n->additional_second.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main_second.name, right_table_name);
    strcpy(n->additional_second.name, right_entity_name);

    n->left = left_column;
    n->right = right_column;
    n->center = column_list;
    return n;
}

struct node *new_delete_query(const char *table_name, const char *entity_name, struct node *filter_statement) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = DELETE_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    n->additional.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table_name);
    strcpy(n->additional.name, entity_name);
    n->left = filter_statement;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_update_query(const char *table_name, const char *entity_name, struct node *column_list, struct node *filter_statement,
                       node *value_list) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = UPDATE_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    n->additional.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table_name);
    strcpy(n->additional.name, entity_name);
    n->left = column_list;
    n->right = filter_statement;
    n->center = value_list;
    return n;
}

struct node *new_insert_query(const char *table_name, struct node *value_list) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = INSERT_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table_name);
    n->left = value_list;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_create_query(const char *table_name, struct node *initialize_column_list) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = CREATE_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table_name);
    n->left = initialize_column_list;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_drop_query(const char *table_name) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = DROP_QUERY;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, table_name);
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_list(struct node *first_part, struct node *second_part) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = CREATE_LIST;
    n->left = first_part;
    n->right = second_part;
    n->center = NULL;
    return n;
}

struct node *old_column(const char* entity_name, const char* column_name) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = OLD_COLUMN;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, entity_name);
    n->additional.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->additional.name, column_name);
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_column(const char* column_name, struct node *type) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = CREATE_COLUMN;
    n->main.name = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.name, column_name);
    n->left = type;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_type(enum data_type type) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = TYPE_NODE;
    n->main.data_type = type;
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_varchar(const char *varchar_val) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = VARCHAR_NODE;
    n->main.varchar_val = (char*) malloc(sizeof(char) * 255);
    strcpy(n->main.varchar_val, varchar_val);
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_int(int int_val) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = INT_NODE;
    n->main.int_val = int_val;
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_double(double double_val) {
    node *n = (struct node*) malloc(sizeof(struct node));
    n->type = DOUBLE_NODE;
    n->main.double_val = double_val;
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_boolean(bool boolean_val) {
    node *n = (struct node*) malloc(sizeof(struct node));
    n->type = BOOLEAN_NODE;
    n->main.boolean_val = boolean_val;
    n->left = NULL;
    n->right = NULL;
    n->center = NULL;
    return n;
}

struct node *new_filter_compare_statement(enum compare_type cmp_type, struct node *first_operand, struct node *second_operand) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = FILTER_CMP;
    n->main.compare_type = cmp_type;
    n->left = first_operand;
    n->right = second_operand;
    n->center = NULL;
    return n;
}

struct node *new_filter_logic_statement(enum logic_operator logic_op, struct node *first_operand, struct node *second_operand) {
    struct node *n = (struct node*) malloc(sizeof(struct node));
    n->type = FILTER_LOGIC;
    n->main.logic_operator = logic_op;
    n->left = first_operand;
    n->right = second_operand;
    n->center = NULL;
    return n;
}
